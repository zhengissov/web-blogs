export default function SportLevelsEasy() {
  return (
    <svg width="49" height="55" viewBox="0 0 49 55" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M17.9353 54.5207C17.9353 54.5207 19.446 37.9269 17.4973 29.617C15.4025 20.6774 13.0539 19.2751 11.1496 20.1645C8.14074 21.5408 14.5456 26.6046 16.8054 35.5378C18.843 43.6204 16.1135 54.5207 16.1135 54.5207H17.9353Z"
        fill="#28CA7C"
      />
      <path
        d="M23.4268 30.3318C22.2276 32.9809 20.9871 36.9303 20.3806 39.7829C19.384 44.4832 18.0615 54.0284 18.0615 54.0284"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="square"
      />
      <path
        d="M23.4268 30.3318C22.2276 32.9809 20.9871 36.9303 20.3806 39.7829C19.384 44.4832 18.0615 54.0284 18.0615 54.0284"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="square"
      />
      <path
        d="M6.00244 27.8252C6.00244 27.8252 8.30666 32.5969 13.7149 33.0189C15.3271 33.1138 16.9397 32.8409 18.4361 32.22C19.9324 31.5991 21.2751 30.6457 22.3668 29.4288"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M6.00244 27.8252C6.00244 27.8252 8.30666 32.5969 13.7149 33.0189C15.3271 33.1138 16.9397 32.8409 18.4361 32.22C19.9324 31.5991 21.2751 30.6457 22.3668 29.4288"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M7.00621 23.6505C7.00621 23.6505 5.26694 26.4032 6.0604 27.9483"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M7.00621 23.6505C7.00621 23.6505 5.26694 26.4032 6.0604 27.9483"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M3.76123 22.7526C3.76123 22.7526 4.01514 25.1871 5.66554 26.5569"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M3.76123 22.7526C3.76123 22.7526 4.01514 25.1871 5.66554 26.5569"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M1.72314 23.9856C2.00145 24.4717 2.37338 24.895 2.81637 25.2298C3.25937 25.5646 3.76421 25.8039 4.30031 25.9332"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M1.72314 23.9856C2.00145 24.4717 2.37338 24.895 2.81637 25.2298C3.25937 25.5646 3.76421 25.8039 4.30031 25.9332"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M3.58987 27.244C2.63383 27.1982 1.72106 26.823 1 26.1793"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M3.58987 27.244C2.63383 27.1982 1.72106 26.823 1 26.1793"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M48.3674 13.0163C48.6149 11.8672 47.1359 11.4777 44.1462 12.3022C42.0771 12.9264 40.064 13.73 38.1285 14.7042C38.9636 12.8434 39.6596 10.9206 40.2106 8.95222C41.4801 3.88188 40.6613 1.87584 40.2106 1.21364C39.8741 0.739718 39.0616 -0.201635 36.9923 0.0385728C35.1578 0.252812 32.5108 1.81742 29.8194 4.8752C28.3848 6.50471 26.4107 8.97821 26.4107 8.97821C26.4107 8.97821 26.0171 7.45256 25.6299 6.12817C25.2681 4.91414 24.6206 1.58367 22.7671 1.07079C21.0913 0.603363 20.228 2.64189 19.8027 4.73235C19.3774 6.82281 19.3203 16.0286 20.7422 25.559C21.4531 30.3112 28.3911 27.7209 29.8194 29.292C32.0013 28.2773 34.1211 27.1282 36.1671 25.8512C39.9746 23.3149 43.3967 20.2197 46.3234 16.6648C47.1999 15.5745 47.8909 14.3411 48.3674 13.0163Z"
        fill="#D8E3FF"
      />
      <path
        d="M24.2896 19.8312C24.2896 19.8312 25.3941 24.4471 32.2496 21.7788"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M24.2896 19.8312C24.2896 19.8312 25.3941 24.4471 32.2496 21.7788"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M23.3308 14.0292C23.3308 14.0292 23.2293 16.626 27.0125 15.9119"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M23.3308 14.0292C23.3308 14.0292 23.2293 16.626 27.0125 15.9119"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M29.5633 16.4785C29.5633 16.4785 29.4617 19.0754 33.2449 18.3612"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M29.5633 16.4785C29.5633 16.4785 29.4617 19.0754 33.2449 18.3612"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M20.7275 25.5711C21.083 28.7133 21.4321 30.2519 22.2193 31.1348C23.4253 32.2255 27.1451 31.1348 29.8365 29.3041C28.6846 28.0289 27.2695 27.0322 25.6938 26.3865C24.1182 25.7408 22.4216 25.4622 20.7275 25.5711Z"
        fill="#28CA7C"
      />
      <path
        d="M20.6729 54.5225C20.6729 54.5225 26.4612 29.7008 37.4998 24.292C48.5385 18.8833 46.776 36.2271 46.0277 37.7789C45.3536 39.2093 41.8844 42.2937 41.8102 30.8375C41.7483 21.0353 36.9742 25.1477 35.1808 27.006C30.9199 31.3611 27.6671 43.6602 25.0759 54.548L20.6729 54.5225Z"
        fill="#28CA7C"
      />
      <path
        d="M32.5857 42.879C32.5857 42.879 23.8893 39.5485 25.2985 30.4207"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M32.5857 42.879C32.5857 42.879 23.8893 39.5485 25.2985 30.4207"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M36.503 41.3212C36.503 41.3212 34.0654 43.4571 32.4658 42.8598"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M36.503 41.3212C36.503 41.3212 34.0654 43.4571 32.4658 42.8598"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M37.7979 44.1769C36.486 44.1275 35.2317 43.6134 34.2495 42.7227"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M37.7979 44.1769C36.486 44.1275 35.2317 43.6134 34.2495 42.7227"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M37.1817 46.0766C36.6238 45.9665 36.1022 45.7142 35.665 45.3431C35.2279 44.9721 34.8893 44.4942 34.6807 43.9537"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M37.1817 46.0766C36.6238 45.9665 36.1022 45.7142 35.665 45.3431C35.2279 44.9721 34.8893 44.4942 34.6807 43.9537"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M33.2212 44.5092C33.2971 44.9822 33.4655 45.4347 33.7164 45.8396C33.9672 46.2446 34.2954 46.5936 34.6812 46.8658"
        stroke="#2F62FF"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M33.2212 44.5092C33.2971 44.9822 33.4655 45.4347 33.7164 45.8396C33.9672 46.2446 34.2954 46.5936 34.6812 46.8658"
        stroke="#1B17C0"
        strokeWidth="1.54143"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
    </svg>
  );
}
