export default function Facebook() {
  return (
    <svg width="58" height="58" viewBox="0 0 58 58" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="29.1959" cy="28.8043" r="28.8043" fill="#EDF2FF" />
      <path
        d="M26.1444 42H31.0806V30.1375H35.2082L35.8315 25.5225H31.0806V22.565C31.0806 21.2325 31.4681 20.3225 33.4561 20.3225H36V16.1787C35.562 16.13 34.0626 16 32.3105 16C28.6378 16 26.1444 18.1613 26.1444 22.11V25.5225H22V30.1375H26.1444V42Z"
        fill="#1F5EFF"
      />
    </svg>
  );
}
