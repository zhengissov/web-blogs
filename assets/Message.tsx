export default function Message() {
  return (
    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M7 0C10.8598 0 14 2.5123 14 5.6C14 7.45017 12.9024 9.12628 11.0721 10.1621C10.7315 10.3548 10.5 10.7047 10.5 11.096V12C10.5 12.824 9.55924 13.2944 8.9 12.8L7.01686 11.3876C6.85106 11.2633 6.65043 11.1953 6.44359 11.1822C2.84304 10.9552 0 8.53791 0 5.6C0 2.5123 3.1402 0 7 0Z"
        fill="#1F5EFF"
      />
      <rect x="3" y="6" width="6" height="1" rx="0.5" fill="white" />
      <rect x="3" y="4" width="8" height="1" rx="0.5" fill="white" />
    </svg>
  );
}
