export default function ChevronRight() {
  return (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle
        className="chevron-right__circle"
        opacity="0.44"
        cx="10"
        cy="10"
        r="10"
        fill="transparent"
      />
      <path
        d="M9.25 6.5L12.5002 9.75019L9.25 13.0004"
        stroke="white"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
