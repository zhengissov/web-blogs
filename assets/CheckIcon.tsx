export default function CheckIcon() {
  return (
    <svg
      className="check-icon"
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="10" cy="10" r="9.5" fill="#1F5EFF" stroke="#1F5EFF" strokeWidth="1" />
      <path d="M6.80127 9.4292L9.45273 11.8387L13.6442 7.64722" stroke="white" strokeWidth="2" />
    </svg>
  );
}
