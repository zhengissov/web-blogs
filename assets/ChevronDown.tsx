export default function ChevronDown() {
  return (
    <svg
      className="chevron-down"
      width="22"
      height="14"
      viewBox="0 0 22 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M2 2L11.4018 11L20.8036 2" stroke="#1F5EFF" strokeWidth="3" />
    </svg>
  );
}
