export default function CloseButton() {
  return (
    <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="20" cy="20" r="20" fill="#E6EDFF" />
      <path d="M10.9097 10.9082L28.5966 28.5952" stroke="#0029A7" strokeWidth="2" />
      <path d="M28.5962 10.9082L10.9092 28.5952" stroke="#0029A7" strokeWidth="2" />
    </svg>
  );
}
