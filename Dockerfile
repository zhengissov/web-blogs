FROM node:14.17.0-alpine AS base

WORKDIR /app

COPY package.json ./

#RUN yarn install --production
RUN yarn install
ENV NODE_ENV production

RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001

COPY . .

ENV NEXT_PUBLIC_API_URL https://api.1fit.app/api
ENV NEXT_PUBLIC_SITE_URL https://blog.1fit.app

RUN yarn build

USER nextjs

EXPOSE 3000

CMD [ "yarn", "start" ]