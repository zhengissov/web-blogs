const { i18n } = require('./next-i18next.config');

/** @type {import('next').NextConfig} */
module.exports = {
  i18n,
  images: {
    domains: ['onefit-static-0286.s3.amazonaws.com', 'onefit-static.s3.amazonaws.com'],
  },
  async rewrites() {
    return [
      {
        source: '/',
        destination: '/category/all',
      },
      {
        source: '/category',
        destination: '/category/all',
      },
    ];
  },
};
