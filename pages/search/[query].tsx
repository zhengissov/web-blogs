import type { GetStaticProps, NextPage } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { BlogsSearch } from 'components/BlogsSearch';
import { Header } from 'components/Header';
import { Footer } from 'components/Footer';
import { SubscriptionForm } from 'components/SubscriptionForm';
import { GetStaticPaths } from 'next';

const SearchPage: NextPage = () => {
  return (
    <>
      <Header />
      <main className="container">
        <BlogsSearch />
        <SubscriptionForm />
      </main>
      <Footer />
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale, defaultLocale }) => ({
  props: {
    ...(await serverSideTranslations(locale || defaultLocale || 'ru', [
      'common',
      'download-app',
      'level',
    ])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => {
  return { paths: [], fallback: 'blocking' };
};

export default SearchPage;
