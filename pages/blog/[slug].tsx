import type { GetServerSideProps, NextPage } from 'next';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Link from 'next/link';
import { motion } from 'framer-motion';
import { NextSeo, BlogJsonLd } from 'next-seo';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { BlogDetail } from 'store/blog/types';
import { Header } from 'components/Header';
import { Footer } from 'components/Footer';
import { SubscriptionForm } from 'components/SubscriptionForm';
import { PageScrollProgress } from 'components/PageScrollProgress';
import { Tag } from 'components/Tag';
import { parseDate } from 'helpers/date';
import { fadeInUpDetail, stagger } from 'styles/animations';
import { SimilarBlogs } from 'components/SimilarBlogs';
import { parseLevel } from 'helpers/level';
import { useRouter } from 'next/router';
import { request } from "../../store/axios";
import { parseBlogDetail } from "../../store/blog/parsers";

type Props = {
  blog: BlogDetail;
  slug: string;
};

const BlogDetailPage: NextPage<Props> = ({ blog }) => {
  const router = useRouter();

  const {
    level,
    category,
    title,
    author,
    viewedCount,
    content,
    createdAt,
    image,
    similarBlogs,
    estimatedReadTime,
  } = blog;

  const slug = title;
  const { t } = useTranslation(['common', 'level']);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <NextSeo
        title={`${title} | 1Fit Blogs`}
        description={`${title} | 1Fit Blogs - все про здоровье, питание и здоровый образ жизни`}
        openGraph={{
          title: `${title} | 1Fit Blogs`,
          description: `${title} | 1Fit Blogs - все про здоровье, питание и здоровый образ жизни`,
          url: `${process.env.NEXT_PUBLIC_SITE_URL}/blog/${slug}`,
          site_name: '1Fit',
          images: [{ url: image }],
          type: 'article',
          article: {
            publishedTime: createdAt,
            modifiedTime: createdAt,
            authors: [author],
            section: category.name,
          },
        }}
        languageAlternates={[
          {
            hrefLang: 'ru',
            href: `${process.env.NEXT_PUBLIC_SITE_URL}/ru/blog/${slug}`,
          },
          {
            hrefLang: 'kk',
            href: `${process.env.NEXT_PUBLIC_SITE_URL}/kk/blog/${slug}`,
          },
        ]}
      />
      <BlogJsonLd
        url={`${process.env.NEXT_PUBLIC_SITE_URL}/blog/${slug}`}
        title={title}
        images={[image]}
        datePublished={createdAt}
        dateModified={createdAt}
        authorName={author}
        description={`1Fit Blogs | ${title}`}
      />
      <PageScrollProgress />
      <Header />
      <main className="container">
        {blog && (
          <motion.section
            variants={stagger}
            initial="initial"
            animate="animate"
            className="blog-content"
          >
            <motion.div variants={fadeInUpDetail} className="blog__info">
              <Tag>{t(`level:${parseLevel(level)}`)}</Tag>
              <Link href={`/category/${category.slug}`} passHref={true}>
                <a>
                  <Tag variant="subtle">{category.name}</Tag>
                </a>
              </Link>
              <time className="blog__date" dateTime={createdAt}>
                {parseDate(createdAt)}
              </time>
            </motion.div>
            <motion.h1 variants={fadeInUpDetail} className="blog__title">
              {title}
            </motion.h1>

            <motion.div variants={fadeInUpDetail} className="blog__stats">
              <p>{author}</p>
              <p>{t('views', { count: viewedCount })}</p>
              <p>{t('read-in-n-minutes', { count: estimatedReadTime })}</p>
            </motion.div>

            <motion.div variants={fadeInUpDetail} className="blog__image">
              <Image src={image} alt={title} layout="fill" objectFit="cover" />
            </motion.div>

            <motion.div
              variants={fadeInUpDetail}
              className="blog__content"
              dangerouslySetInnerHTML={{ __html: content }}
            />
          </motion.section>
        )}
      </main>
      <>{similarBlogs && similarBlogs.length > 0 && <SimilarBlogs blogs={similarBlogs} />}</>
      <main className="container">
        <SubscriptionForm />
      </main>
      <Footer />
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ locale, defaultLocale }) => {
  const response = await request.get(`https://api.1fit.app/api/studios/kaisarov-functional-training`);
  const response2 = await request.get(`https://api.1fit.app/api/blogs/chto-takoe-cigun/`);
  // const blog: BlogDetail = parseBlogDetail(response.data.result);
  const blog = {
    "title": response.data.result.title,
    "content": response2.data.result.title,
    "category": {
      "name": "Развлечение",
      "slug": "razvlechenie"
    },
    "level": "NEW",
    "author": "Nurlan Tolegenov",
    "createdAt": "2021-11-19T16:06:16.157230+06:00",
    "image": "https://onefit-static-0286.s3.amazonaws.com/media/blogs/%D0%9F%D0%B0%D0%BC%D1%8F%D1%82%D1%8C_%D0%A2%D0%B8%D0%BB%D1%8C%D0%B4%D1%8B_%D0%A1%D1%83%D0%B8%D0%BD%D1%82%D0%BE%D0%BD.jpeg",
    "country": 1,
    "tags": [],
    "viewedCount": 37,
    "similarBlogs": [],
    "estimatedReadTime": 2
  }

  return {
    props: {
      ...(await serverSideTranslations(locale || defaultLocale || 'ru', [
        'common',
        'download-app',
        'level',
      ])),
      blog,
    },
  };
};

export default BlogDetailPage;
