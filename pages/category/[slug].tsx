import type { GetStaticProps, NextPage, GetStaticPaths } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Blogs } from 'components/Blogs';
import { Categories } from 'components/Categories';
import { Header } from 'components/Header';
import { Footer } from 'components/Footer';
import { SubscriptionForm } from 'components/SubscriptionForm';
import { getCategoriesFetcher } from 'store/categories/api';
import { EditorsModal } from 'components/EditorsModal';

const CategoryPage: NextPage = () => {
  return (
    <>
      <Header />
      <main className="container blogs-main">
        <Categories />
        <Blogs />
        <div className="editors-modal__section--mobile">
          <EditorsModal />
        </div>
        <SubscriptionForm />
      </main>
      <Footer />
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale, defaultLocale }) => ({
  props: {
    ...(await serverSideTranslations(locale || defaultLocale || 'ru', [
      'common',
      'download-app',
      'level',
    ])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => {
  const categories = await getCategoriesFetcher(`blogs/categories/`);
  const paths = categories.map((c) => ({
    params: {
      slug: c.slug,
    },
  }));
  return { paths, fallback: 'blocking' };
};

export default CategoryPage;
