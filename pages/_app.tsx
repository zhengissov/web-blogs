import { useEffect, useState } from 'react';
import type { AppProps } from 'next/app';
import { parseCookies } from 'nookies';
import { appWithTranslation } from 'next-i18next';
import { DefaultSeo } from 'next-seo';
import { SportLevelContext } from 'helpers/context';
import 'styles/index.css';

function MyApp({ Component, pageProps }: AppProps) {
  const [level, setLevel] = useState('none');
  const { level: cookieLevel } = parseCookies();

  useEffect(() => {
    if (cookieLevel) {
      setLevel(cookieLevel);
    }
  }, [cookieLevel]);

  return (
    <SportLevelContext.Provider value={{ level, setLevel }}>
      <DefaultSeo
        title="1Fit Blogs"
        description="1Fit Blogs - все про здоровье, питание и здоровый образ жизни"
        openGraph={{
          url: process.env.NEXT_PUBLIC_SITE_URL,
          title: '1Fit Blogs',
          description: '1Fit Blogs - все про здоровье, питание и здоровый образ жизни',
          type: 'website',
          site_name: '1Fit Blogs',
          images: [{ url: `${process.env.NEXT_PUBLIC_SITE_URL}/1fit.png` }],
        }}
        twitter={{
          handle: '@1fit4',
          site: process.env.NEXT_PUBLIC_SITE_URL,
          cardType: 'summary_large_image',
        }}
        languageAlternates={[
          {
            hrefLang: 'ru',
            href: `${process.env.NEXT_PUBLIC_SITE_URL}/ru`,
          },
          {
            hrefLang: 'kk',
            href: `${process.env.NEXT_PUBLIC_SITE_URL}/kk`,
          },
        ]}
        additionalLinkTags={[
          {
            rel: 'icon',
            type: 'image/png',
            sizes: '32x32',
            href: '/favicon-32x32.png',
          },
          {
            rel: 'icon',
            type: 'image/png',
            sizes: '16x16',
            href: '/favicon-16x16.png',
          },
          {
            rel: 'apple-touch-icon',
            href: '/apple-touch-icon.png',
            sizes: '180x180',
          },
          {
            rel: 'manifest',
            href: '/manifest.json',
          },
          {
            rel: 'mask-icon',
            color: '#5bbad5',
            href: '/safari-pinned-tab.svg',
          },
        ]}
        additionalMetaTags={[
          {
            property: 'keyword',
            content: 'Блог,Статьи,Фитнес,Здоровье,Питание,Здоровый Образ Жизни,Развлечение',
          },
          {
            property: 'msapplication-TileColor',
            content: '#da532c',
          },
          {
            property: 'theme-color',
            content: '#ffffff',
          },
          {
            name: 'apple-itunes-app',
            content: 'app-id=1375903148',
          },
        ]}
      />
      <Component {...pageProps} />
    </SportLevelContext.Provider>
  );
}

export default appWithTranslation(MyApp);
