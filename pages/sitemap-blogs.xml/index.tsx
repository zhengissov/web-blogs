import { getServerSideSitemap } from 'next-sitemap';
import { GetServerSideProps } from 'next';
import axios from 'axios';
import { BlogResponse } from 'store/blog/types';
import { ResponsePaginationData } from 'store/types';

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/blogs/?limit=1000`);
  const blogs: ResponsePaginationData<BlogResponse> = response.data;

  const alternateRefs = [
    {
      href: `${process.env.NEXT_PUBLIC_SITE_URL}/ru`,
      hreflang: 'ru',
    },
    {
      href: `${process.env.NEXT_PUBLIC_SITE_URL}/kk`,
      hreflang: 'kk',
    },
  ];

  const fields = blogs.result.results.map((n) => ({
    loc: `${process.env.NEXT_PUBLIC_SITE_URL}/${n.slug}`,
    lastmod: new Date().toISOString(),
    alternateRefs: alternateRefs.map((ar) => ({
      ...ar,
      href: `${ar.href}/${n.slug}`,
    })),
  }));

  return getServerSideSitemap(ctx, fields);
};

function SiteMapPage(): JSX.Element {
  return <></>;
}

export default SiteMapPage;
