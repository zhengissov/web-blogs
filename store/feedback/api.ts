import { ResponseData } from 'store/types';
import { post } from 'store/axios';

type FeedbackRequestData = {
  email: string;
  message: string;
};

export const sendFeedback = (email: string, message: string) =>
  post<FeedbackRequestData, ResponseData<string>>(`/blogs/feedback/`, { email, message }).then(
    (res) => res.data,
  );
