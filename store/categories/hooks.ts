import useSWR from 'swr';
import { getCategoriesFetcher } from './api';

export const useCategories = () => {
  const { data, error } = useSWR('blogs/categories/', getCategoriesFetcher);
  return {
    data,
    error,
    isLoading: !error && !data,
  };
};
