import { ResponseData } from 'store/types';
import { get } from 'store/axios';
import { Category } from './types';

export const getCategoriesFetcher = (url: string) =>
  get<ResponseData<Category[]>>(url).then((res) => res.data.result);
