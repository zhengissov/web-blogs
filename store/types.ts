export type ResponsePaginationData<T> = {
  result: {
    count: number;
    next: number | null;
    results: T[];
  };
};

export type ResponseData<T> = {
  code: number;
  message: string;
  result: T;
};
