import { ResponsePaginationData } from 'store/types';
import {
  BlogResponse,
  Blog,
  BlogsState,
  BlogDetailResponse,
  BlogDetail,
  BlogSearchResponse,
  BlogSearch,
} from './types';

export const parseBlog = (raw: BlogResponse): Blog => ({
  title: raw.title,
  category: raw.category,
  createdAt: raw.created_at,
  image: raw.image,
  level: raw.level,
  slug: raw.slug,
});

export const parseBlogs = (raw: ResponsePaginationData<BlogResponse>): BlogsState => ({
  count: raw.result.count,
  next: raw.result.next,
  results: raw.result.results.map(parseBlog),
});

export const parseBlogDetail = (raw: BlogDetailResponse): BlogDetail => ({
  title: raw.title,
  content: raw.content,
  category: raw.category,
  level: raw.level,
  author: raw.author,
  image: raw.image,
  createdAt: raw.created_at,
  viewedCount: raw.viewed_count,
  similarBlogs: raw.similar_blogs.map(parseBlog),
  estimatedReadTime: raw.estimated_read_time,
});

export const parseBlogSearch = (raw: BlogSearchResponse): BlogSearch => ({
  searchResults: raw.search_results.map(parseBlog),
  otherBlogs: raw.other_blogs.map(parseBlog),
});
