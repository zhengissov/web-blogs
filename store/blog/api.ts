import { get } from 'store/axios';
import { ResponseData, ResponsePaginationData } from 'store/types';
import { parseBlogDetail, parseBlogs, parseBlogSearch } from './parsers';
import { BlogDetailResponse, BlogResponse, BlogSearchResponse, BlogSlug } from './types';

export const getBlogsFetcher = (url: string) =>
  get<ResponsePaginationData<BlogResponse>>(url).then((res) => parseBlogs(res.data));

export const getBlogDetailFetcher = (url: string) =>
  get<ResponseData<BlogDetailResponse>>(url).then((res) => parseBlogDetail(res.data.result));

export const getBlogSlugsFetcher = (url: string) =>
  get<ResponseData<BlogSlug[]>>(url).then((res) => res.data.result);

export const getBlogSearchFetcher = (url: string) =>
  get<ResponseData<BlogSearchResponse>>(url).then((res) => parseBlogSearch(res.data.result));
