export type BlogResponse = {
  title: string;
  category: Category;
  created_at: string;
  image: string;
  level: string;
  slug: string;
};

export type Blog = {
  title: string;
  category: Category;
  createdAt: string;
  image: string;
  level: string;
  slug: string;
};

export type BlogsState = {
  count: number;
  next: number | null;
  results: Blog[];
};

type Category = {
  name: string;
  slug: string;
};

export type BlogDetailResponse = {
  title: string;
  content: string;
  category: Category;
  level: string;
  author: string;
  image: string;
  created_at: string;
  viewed_count: number;
  similar_blogs: BlogResponse[];
  estimated_read_time: number;
};

export type BlogDetail = {
  title: string;
  content: string;
  category: Category;
  level: string;
  author: string;
  image: string;
  createdAt: string;
  viewedCount: number;
  similarBlogs: Blog[];
  estimatedReadTime: number;
};

export type BlogSlug = {
  slug: string;
};

export type BlogSearchResponse = {
  other_blogs: BlogResponse[];
  search_results: BlogResponse[];
};

export type BlogSearch = {
  searchResults: Blog[];
  otherBlogs: Blog[];
};
