import useSWR from 'swr';
import useSWRInfinite from 'swr/infinite';
import { getBlogSearchFetcher, getBlogsFetcher } from './api';
import { BlogsState } from './types';

export const useBlogs = (
  isReady: boolean,
  category: string | string[] | undefined,
  level: string | string[] | undefined,
) => {
  const getKey = (isReady: boolean) => (index: number, previousPageData: BlogsState) => {
    if (!isReady) return null;
    if (previousPageData && !previousPageData.next) return null;
    return `blogs/?limit=6&page=${index + 1}${category !== 'all' ? `&category=${category}` : ''}${
      level !== 'all' ? `&level=${level}` : ''
    }`;
  };

  const { data, error, setSize } = useSWRInfinite<BlogsState>(getKey(isReady), getBlogsFetcher, {
    revalidateFirstPage: false,
  });

  return {
    data,
    error,
    setSize,
  };
};

export const useBlogSearch = (query: string | string[] | undefined) => {
  const { data, error } = useSWR(`blogs/search/?query=${query}`, getBlogSearchFetcher);
  return {
    data,
    error,
    isLoading: !error && !data,
  };
};
