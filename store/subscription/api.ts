import { ResponseData } from 'store/types';
import { post } from 'store/axios';

type SubscriptionData = {
  email: string;
};

export const subscribe = (email: string) =>
  post<SubscriptionData, ResponseData<SubscriptionData>>(`/blogs/subscribe/`, { email }).then(
    (res) => res.data,
  );
