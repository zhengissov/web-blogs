import axios, { AxiosResponse } from 'axios';

export const request = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export const get = <T>(url: string): Promise<AxiosResponse<T>> => request.get(url);

export const post = <Req, Res>(url: string, data: Req): Promise<AxiosResponse<Res>> =>
  request.post(url, data);
