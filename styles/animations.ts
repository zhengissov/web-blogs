export const stagger = {
  animate: {
    transition: {
      staggerChildren: 0.1,
    },
  },
};

const easing = [0.6, -0.05, 0.1, 0.9];

export const fadeInUpCategories = {
  initial: {
    y: 10,
    opacity: 0,
  },
  animate: {
    y: 0,
    opacity: 1,
    transition: {
      duration: 0.2,
      ease: 'circIn',
    },
  },
};

export const fadeInUpDetail = {
  initial: {
    y: 60,
    opacity: 0,
  },
  animate: {
    y: 0,
    opacity: 1,
    transition: {
      duration: 0.6,
      ease: easing,
    },
  },
};
