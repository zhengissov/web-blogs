import { useContext } from 'react';
import { useTranslation } from 'next-i18next';
import SportLevelsNone from 'assets/SportLevelsNone';
import SportLevelsAll from 'assets/SportLevelsAll';
import SportLevelsEasy from 'assets/SportLevelsEasy';
import SportLevelsNormal from 'assets/SportLevelsNormal';
import SportLevelsHard from 'assets/SportLevelsHard';
import ChevronRight from 'assets/ChevronRight';
import { SportLevelContext } from 'helpers/context';

type Props = {
  onClick: () => void;
};

export const LevelModalButton = ({ onClick }: Props) => {
  const { t } = useTranslation('level');
  const { level } = useContext(SportLevelContext);
  const isSpecificLevel = level !== 'none' && level !== 'all';

  return (
    <div onClick={onClick} className="level-modal__button">
      {level === 'none' && <SportLevelsNone />}
      {level === 'all' && <SportLevelsAll />}
      {isSpecificLevel && (
        <div className="level-modal__button__icon__container">
          <div className="level-modal__button__icon">
            {level === 'easy' && <SportLevelsEasy />}
            {level === 'normal' && <SportLevelsNormal />}
            {level === 'hard' && <SportLevelsHard />}
          </div>
        </div>
      )}
      <div className="level-modal__button__content">
        <p className="level-modal__button__title">{t(level)}</p>
        {isSpecificLevel && <p className="level-modal__button__description">Уровень {level}</p>}
      </div>
      <ChevronRight />
    </div>
  );
};
