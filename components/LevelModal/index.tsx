import { useContext, useState } from 'react';
import Modal from 'react-modal';
import { setCookie } from 'nookies';
import { useTranslation } from 'next-i18next';
import { LevelModalButton } from './button';
import SportLevelsEasy from 'assets/SportLevelsEasy';
import SportLevelsNormal from 'assets/SportLevelsNormal';
import SportLevelsHard from 'assets/SportLevelsHard';
import CloseButton from 'assets/CloseButton';
import CheckIcon from 'assets/CheckIcon';
import { SportLevelContext } from 'helpers/context';

Modal.setAppElement('#level-modal');

const sportLevels = ['easy', 'normal', 'hard'];

export const LevelModal = () => {
  const { t } = useTranslation('level');
  const [modalIsOpen, setIsOpen] = useState(false);
  const { level, setLevel } = useContext(SportLevelContext);
  const [selectedLevel, setSelectedLevel] = useState(level);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const changeLevel = (l: string) => () => {
    setCookie(null, 'level', l, {
      maxAge: 30 * 24 * 60 * 60,
      path: '/',
    });
    setLevel(l);
    setSelectedLevel(l);
  };

  return (
    <div id="level-modal" className="level-modal-wrap">
      <LevelModalButton onClick={openModal} />
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Level modal"
        className="level-modal"
        overlayClassName="level-modal__overlay"
      >
        <div className="level-modal__close-button" onClick={closeModal}>
          <CloseButton />
        </div>
        <h2 className="level-modal__title">Выберите свой спортивный уровень</h2>
        {sportLevels.map((sportLevel) => (
          <div
            key={sportLevel}
            onClick={changeLevel(sportLevel)}
            className={`level-modal__level-option ${
              sportLevel === selectedLevel ? 'level-modal__level-option--selected' : ''
            }`}
          >
            <div className="level-modal__level-icon">
              {sportLevel === 'easy' && <SportLevelsEasy />}
              {sportLevel === 'normal' && <SportLevelsNormal />}
              {sportLevel === 'hard' && <SportLevelsHard />}
            </div>
            <div className="level-modal__level-content">
              <p className="level-modal__level-title">{t(`${sportLevel}-title`)}</p>
              <p className="level-modal__level-description">{t(`${sportLevel}-description`)}</p>
            </div>
            <CheckIcon />
          </div>
        ))}
        <p className="level-modal__select-all-levels" onClick={changeLevel('all')}>
          К чёрту условности, хочу читать все
        </p>
      </Modal>
    </div>
  );
};
