import { useEffect, useState } from 'react';
import { motion, useViewportScroll } from 'framer-motion';

export const PageScrollProgress = () => {
  const { scrollYProgress } = useViewportScroll();
  const [scrollPosition, setScrollPosition] = useState(0);

  useEffect(() => {
    scrollYProgress.onChange((v) => setScrollPosition(v));
    return () => {
      scrollYProgress.destroy();
    };
  }, [scrollYProgress]);

  return (
    <motion.div
      className="scroll-progress"
      initial={{ width: '0' }}
      animate={{ width: `calc(${scrollPosition * 100}%)` }}
    />
  );
};
