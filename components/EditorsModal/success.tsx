import Flower from 'assets/Flower';

export function SuccessMessage() {
  return (
    <div className="editors-modal__success-message">
      <Flower />
      <p className="editors-modal__success-message__title">Спасибо за обратную связь</p>
      <p className="editors-modal__success-message__description">Мы получили ваше сообщение</p>
    </div>
  );
}
