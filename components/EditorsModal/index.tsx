import { ChangeEvent, FormEvent, useState } from 'react';
import Modal from 'react-modal';
import Message from 'assets/Message';
import FeedbackFlower from 'assets/FeedbackFlower';
import CloseButton from 'assets/CloseButton';
import { Loader } from 'components/Loader';
import { SuccessMessage } from './success';
import { sendFeedback } from 'store/feedback/api';

Modal.setAppElement('#editors-modal');

export const EditorsModal = () => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isFailure, setIsFailure] = useState(false);
  const [errorMessage, setErrorMessage] = useState('Произошла ошибка, попробуйте еще раз');

  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const handleMessageChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setMessage(e.target.value);
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    setIsLoading(true);
    sendFeedback(email, message)
      .then(() => {
        setIsLoading(false);
        setIsSuccess(true);
      })
      .catch((err) => {
        setIsLoading(false);
        setIsFailure(true);
        if (err?.response.data.message) {
          setErrorMessage(err?.response.data.message);
        }
      });
  };

  return (
    <div id="editors-modal" className="editors-modal-wrap">
      <div className="editors-modal__button" onClick={openModal}>
        <Message />
        <p className="editors-modal__button__text">Редакция на связи</p>
      </div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Editors modal"
        className="editors-modal"
        overlayClassName="editors-modal-overlay"
      >
        <div className="editors-modal__header">
          <h2 className="editors-modal__title">Редакция на связи</h2>
          <div className="editors-modal__background-flowers">
            <FeedbackFlower />
          </div>
          <div className="editors-modal__close-button" onClick={closeModal}>
            <CloseButton />
          </div>
        </div>
        <div className="editors-modal__body">
          {isLoading ? (
            <div className="editors-modal__loader">
              <Loader />
            </div>
          ) : isSuccess ? (
            <SuccessMessage />
          ) : (
            <>
              <p className="editors-modal__desc">
                Если вы хотите поделиться своим опытом и стать героем публикации, покритиковать
                напрямую качество наших материалов или предложить сотрудничество, пожалуйста,
                пишите.
              </p>
              <form onSubmit={handleSubmit} className="editors-modal__form">
                <input
                  type="email"
                  required={true}
                  value={email}
                  onChange={handleEmailChange}
                  className="editors-modal__input"
                  placeholder="Ваш email"
                />
                <textarea
                  required={true}
                  value={message}
                  onChange={handleMessageChange}
                  className="editors-modal__textarea"
                  placeholder="Сообщение"
                />
                {isFailure && <p className="editors-modal__error-message">{errorMessage}</p>}
                <button type="submit" className="editors-modal__submit">
                  Отправить
                </button>
              </form>
            </>
          )}
        </div>
      </Modal>
    </div>
  );
};
