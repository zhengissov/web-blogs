import { useRouter } from 'next/router';
import { motion } from 'framer-motion';
import { useBlogSearch } from 'store/blog/hooks';
import { BlogCard } from 'components/BlogCard';
import CryingCactus from 'assets/CryingCactus';
import { Loader } from 'components/Loader';

export const BlogsSearch = () => {
  const router = useRouter();
  const {
    query: { query },
  } = router;

  const { data, isLoading, error } = useBlogSearch(query);

  if (isLoading)
    return (
      <div className="search--loading">
        <Loader />
      </div>
    );
  if (error) return <div>error</div>;

  return (
    <>
      {data?.searchResults && data.searchResults.length > 0 && (
        <>
          <h1 className="search-title">
            Посмотрите, что мы нашли по вашему <br />
            запросу <span>&quot;{query}&quot;</span>
          </h1>
          <div className="search-blogs">
            <motion.div className="search-blogs-grid">
              {data.searchResults.map((blog) => (
                <BlogCard key={blog.slug} {...blog} />
              ))}
            </motion.div>
          </div>
        </>
      )}
      {data?.otherBlogs && data.otherBlogs.length > 0 && (
        <div className="search--failure">
          <CryingCactus />
          <h1 className="search-title">
            По вашему запросу <span>&quot;{query}&quot;</span> мы ничего не нашли.
          </h1>
          <h3 className="search-desc">Но можем предложить вам похожие материалы.</h3>

          <div className="search-blogs">
            <motion.div className="search-blogs-grid">
              {data.otherBlogs.map((blog) => (
                <BlogCard key={blog.slug} {...blog} />
              ))}
            </motion.div>
          </div>
        </div>
      )}
    </>
  );
};
