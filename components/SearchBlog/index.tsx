import { ChangeEvent, KeyboardEvent, useEffect, useState } from 'react';
import { useRouter } from 'next/router';

export const SearchBlog = () => {
  const router = useRouter();
  const { query, push } = router;
  const { query: search } = query;

  const [value, setValue] = useState('');

  useEffect(() => {
    if (search) {
      setValue(`${search}`);
    }
  }, [search]);

  const onKeydown = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      push(
        { pathname: e.currentTarget.value.length > 0 ? `/search/${e.currentTarget.value}` : '/' },
        undefined,
        {
          shallow: true,
        },
      );
    }
  };

  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => setValue(e.target.value);

  return (
    <input
      className="search"
      placeholder="Поиск"
      value={value}
      onChange={onInputChange}
      onKeyDown={onKeydown}
    />
  );
};
