import { useContext } from 'react';
import { useRouter } from 'next/router';
import { AnimatePresence, motion } from 'framer-motion';
import { useBlogs } from 'store/blog/hooks';
import { BlogCard } from 'components/BlogCard';
import ChevronDown from 'assets/ChevronDown';
import { BlogCardSkeleton } from 'components/BlogCard/Skeleton';
import { SportLevelContext } from 'helpers/context';
import { parseLevel } from 'helpers/level';

export const Blogs = () => {
  const router = useRouter();
  const {
    isReady,
    query: { slug: category },
  } = router;
  const { level } = useContext(SportLevelContext);

  const { data, error, setSize } = useBlogs(isReady, category, parseLevel(level));
  if (error) return <div>error</div>;
  if (!data)
    return (
      <div className="blogs">
        <AnimatePresence>
          <motion.div
            className="blogs-grid"
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
          >
            {[...Array(6)].map((_, i) => (
              <BlogCardSkeleton key={i} />
            ))}
          </motion.div>
        </AnimatePresence>
      </div>
    );

  const onClickBtn = () => setSize((prev) => prev + 1);

  const isLoadMoreVisible = data[data.length - 1].next;

  return (
    <div className="blogs">
      <motion.div className="blogs-grid">
        {data.map((n) => n.results.map((blog) => <BlogCard key={blog.slug} {...blog} />))}
      </motion.div>

      {isLoadMoreVisible && (
        <button className="load-more" onClick={onClickBtn}>
          <span className="load-more__text">Показать еще</span>
          <ChevronDown />
        </button>
      )}
    </div>
  );
};
