import Logo from 'assets/Logo';
import Link from 'next/link';
import { EditorsModal } from 'components/EditorsModal';
import { LevelModal } from 'components/LevelModal';
import { SearchBlog } from 'components/SearchBlog';

export const Header = () => {
  return (
    <header className="header container">
      <Link href="/" passHref={true}>
        <a className="header__logo">
          <Logo />
        </a>
      </Link>
      <LevelModal />
      <SearchBlog />
      <EditorsModal />
    </header>
  );
};
