import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper';
import { Blog } from 'store/blog/types';
import { BlogCard } from 'components/BlogCard';
import { SliderControllers } from './controllers';
import 'swiper/css/navigation';
import 'swiper/css/free-mode';
import 'swiper/css';

type Props = {
  blogs: Blog[];
};

export function SimilarBlogs({ blogs }: Props) {
  return (
    <>
      <div className="similar-blogs">
        <div className="similar-blogs__controllers">
          <p className="similar-blogs__title">Также читайте</p>
          <SliderControllers />
        </div>
      </div>
      <Swiper
        className="similar-blogs__slider"
        modules={[Navigation]}
        spaceBetween={16}
        slidesPerView={1.5}
        navigation={{
          nextEl: '.swiper-button--next',
          prevEl: '.swiper-button--prev',
        }}
        breakpoints={{
          '768': {
            slidesPerView: 2.5,
            spaceBetween: 40,
          },
          '992': {
            slidesPerView: 3,
            slidesPerGroup: 3,
          },
        }}
      >
        {blogs.map((blog) => (
          <SwiperSlide key={blog.slug}>
            <BlogCard {...blog} />
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
}
