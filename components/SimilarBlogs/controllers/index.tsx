import PrevSlideButton from './PrevSlide';
import NextSlideButton from './NextSlide';

export function SliderControllers() {
  return (
    <div className="swiper-controllers">
      <PrevSlideButton />
      <NextSlideButton />
    </div>
  );
}
