export default function NextSlide() {
  return (
    <svg
      className="swiper-button--prev"
      width="62"
      height="62"
      viewBox="0 0 62 62"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="31" cy="31" r="31" fill="#E9EFFF" />
      <path d="M34.9001 41.8042L24.4052 31.3092L34.9001 20.8143" stroke="#1F5EFF" strokeWidth="4" />
    </svg>
  );
}
