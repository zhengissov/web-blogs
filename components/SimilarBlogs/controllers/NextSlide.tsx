export default function NextSlide() {
  return (
    <svg
      className="swiper-button--next"
      width="63"
      height="62"
      viewBox="0 0 63 62"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="31.5" cy="31" r="31" fill="#E9EFFF" />
      <path d="M28.7585 20.8142L39.2535 31.3092L28.7585 41.8041" stroke="#1F5EFF" strokeWidth="4" />
    </svg>
  );
}
