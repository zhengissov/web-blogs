import Flower from 'assets/Flower';

export function SuccessMessage() {
  return (
    <div className="success-message">
      <Flower />
      <p className="success-message__title">Спасибо!</p>
      <p className="success-message__description">Будем присылать вам самое интересное.</p>
    </div>
  );
}
