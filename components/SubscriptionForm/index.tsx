import { ChangeEvent, FormEvent, useState } from 'react';
import { subscribe } from 'store/subscription/api';
import { Loader } from 'components/Loader';
import { AxiosError } from 'axios';
import { SuccessMessage } from './success';

export function SubscriptionForm() {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [isSuccess, setIsSuccess] = useState(false);

  const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const resetState = () => {
    setErrorMessage('');
    setEmail('');
  };

  const handleSubscribe = (e: FormEvent) => {
    e.preventDefault();
    setIsLoading(true);
    subscribe(email)
      .then((res) => {
        if (res.code === 0) {
          setIsSuccess(true);
        } else {
          setErrorMessage(res.message || 'Произошла ошибка, пожалуйста попробуйте еще раз');
        }
        setIsLoading(false);
      })
      .catch((err: AxiosError) => {
        setErrorMessage(
          err.response?.data.message || 'Произошла ошибка, пожалуйста попробуйте еще раз',
        );
        setIsLoading(false);
      });
  };

  return (
    <div className="subscription-form">
      {isSuccess ? (
        <SuccessMessage />
      ) : (
        <>
          <h1 className="subscription-form__title">Будьте в курсе всех событий</h1>
          <p className="subscription-form__description">
            Stay up to date with 1Fit company news and media coverage.
          </p>
          {isLoading ? (
            <Loader />
          ) : errorMessage.length > 0 ? (
            <>
              <p className="subscription-form__error-message">{errorMessage}</p>
              <p className="subscription-form__reset" onClick={resetState}>
                Попробовать еще раз
              </p>
            </>
          ) : (
            <form className="subscription-form__form-container" onSubmit={handleSubscribe}>
              <input
                required={true}
                className="subscription-form__subscribe-input"
                type="email"
                value={email}
                onChange={handleEmailChange}
                placeholder="Email"
              />
              <button type="submit" className="subscription-form__subscribe-button">
                Подписаться
              </button>
            </form>
          )}
        </>
      )}
    </div>
  );
}
