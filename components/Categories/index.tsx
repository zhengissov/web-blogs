import { motion } from 'framer-motion';
import { useRouter } from 'next/router';
import { useCategories } from 'store/categories/hooks';
import { Category } from 'store/categories/types';
import { fadeInUpCategories, stagger } from 'styles/animations';
import HomeIcon from 'assets/HomeIcon';

export const Categories = () => {
  const router = useRouter();
  const { query, push } = router;
  const { slug: category } = query;

  const { data } = useCategories();

  const onCategoryClick = (category: Category | null) => () => {
    push({ pathname: `/category/${category?.slug}` }, undefined, {
      shallow: true,
    });
  };

  const resetCategoryFilters = () => {
    push({ pathname: '/category/all' }, undefined, {
      shallow: true,
    });
  };

  return (
    <motion.ul
      variants={stagger}
      initial="initial"
      animate={data && 'animate'}
      className="categories"
    >
      <motion.li
        variants={fadeInUpCategories}
        className={`category ${category === 'all' ? 'category--active' : ''}`}
        onClick={resetCategoryFilters}
      >
        <HomeIcon />
        {category === 'all' && <motion.div className="category__underline" layoutId="underline" />}
      </motion.li>
      {data?.map((cat) => (
        <motion.li
          variants={fadeInUpCategories}
          key={cat.slug}
          className={`category ${cat.slug === category ? 'category--active' : ''}`}
          onClick={onCategoryClick(cat)}
        >
          {cat.name}
          {cat.slug === category ? (
            <motion.div className="category__underline" layoutId="underline" />
          ) : null}
        </motion.li>
      ))}
    </motion.ul>
  );
};
