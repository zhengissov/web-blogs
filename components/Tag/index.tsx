import { ReactNode } from 'react';

type Props = {
  variant?: 'solid' | 'outline' | 'subtle';
  children: ReactNode;
};

export const Tag = (props: Props) => {
  const { variant = 'solid', children } = props;
  return <div className={`tag tag--${variant}`}>{children}</div>;
};
