import { motion } from 'framer-motion';
import { Tag } from 'components/Tag';

export const BlogCardSkeleton = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      className="blog-card blog-card--loading"
    >
      <div className="blog-card__img-wrap" />

      <div className="blog-card__info">
        <Tag>category</Tag>
        <Tag variant="outline">level</Tag>
        <p className="blog-card__date">date</p>
      </div>

      <div>
        <p className="blog-card__title">Blog is loading</p>
      </div>
    </motion.div>
  );
};
