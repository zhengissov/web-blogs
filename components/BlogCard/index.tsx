import Link from 'next/link';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import { motion } from 'framer-motion';
import { Blog } from 'store/blog/types';
import { parseDate } from 'helpers/date';
import { Tag } from 'components/Tag';
import { parseLevel } from 'helpers/level';

export const BlogCard = (props: Blog) => {
  const { t } = useTranslation('level');
  const { title, createdAt, level, category, slug, image } = props;

  return (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} className="blog-card">
      <Link href={`/blog/${slug}`} passHref={true}>
        <a className="blog-card__img-wrap">
          <Image src={image} alt="ss" layout="fill" objectFit="cover" />
        </a>
      </Link>

      <div className="blog-card__info">
        <Tag>{category.name}</Tag>
        <Tag variant="outline">{t(parseLevel(level))}</Tag>
        <time className="blog-card__date" dateTime={createdAt}>
          {parseDate(createdAt)}
        </time>
      </div>

      <Link href={`/blog/${slug}`} passHref={true}>
        <a className="blog-card__title">{title}</a>
      </Link>
    </motion.div>
  );
};
