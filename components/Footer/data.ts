export const companyLinks = [
  {
    title: 'Залы и студии',
    href: 'https://1fit.app/studios',
  },
  {
    title: 'Контакты',
    href: 'https://1fit.app/contacts',
  },
];

export const partnerLinks = [
  {
    title: 'Корпоративные клиенты',
    href: 'https://1fit.app/corporate',
  },
  {
    title: 'Стать нашим партнером',
    href: 'https://1fit.app/partners',
  },
];

export const otherLinks = [
  {
    title: 'Публичная оферта',
    href: 'https://1fit.app/public-offer',
  },
  {
    title: 'Правила пользования',
    href: 'https://1fit.app/terms-of-use',
  },
  {
    title: 'Политика конфиденциальности',
    href: 'https://1fit.app/privacy-policy',
  },
  {
    title: 'FAQ',
    href: 'https://1fit.app/faq',
  },
];
