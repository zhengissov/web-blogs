import Link from 'next/link';
import Logo from 'assets/Logo';
import WhatsApp from 'assets/WhatsApp';
import Telegram from 'assets/Telegram';
import Instagram from 'assets/Instagram';
import Facebook from 'assets/Facebook';
import { companyLinks, otherLinks, partnerLinks } from './data';
import { DownloadApp } from 'components/DownloadApp';

export const Footer = () => {
  return (
    <>
      <DownloadApp />
      <footer className="footer container">
        <div className="footer__content">
          <Link href="/" passHref={true}>
            <a className="footer__logo">
              <Logo />
            </a>
          </Link>
          <div className="footer__section--company">
            <h3 className="footer__title">Компания «1FIT»</h3>
            <ul>
              {companyLinks.map((link) => (
                <li key={link.title}>
                  <a href={link.href} target="_blank" rel="noreferrer" className="footer__link">
                    {link.title}
                  </a>
                </li>
              ))}
            </ul>
          </div>

          <div className="footer__section--partnership">
            <h3 className="footer__title">Сотрудничество</h3>
            <ul>
              {partnerLinks.map((link) => (
                <li key={link.title}>
                  <a href={link.href} target="_blank" rel="noreferrer" className="footer__link">
                    {link.title}
                  </a>
                </li>
              ))}
            </ul>
          </div>

          <div className="footer__section--others">
            <h3 className="footer__title">Разное</h3>
            <ul>
              {otherLinks.map((link) => (
                <li key={link.title}>
                  <a href={link.href} target="_blank" rel="noreferrer" className="footer__link">
                    {link.title}
                  </a>
                </li>
              ))}
            </ul>
          </div>

          <div className="footer__contacts">
            <ul className="footer__socials">
              <li>
                <a href="https://wa.me/77057772776/" target="_blank" rel="noreferrer">
                  <WhatsApp />
                </a>
              </li>
              <li>
                <a href="https://t.me/FitBuster/" target="_blank" rel="noreferrer">
                  <Telegram />
                </a>
              </li>
              <li>
                <a href="https://www.instagram.com/1fit.app/" target="_blank" rel="noreferrer">
                  <Instagram />
                </a>
              </li>
              <li>
                <a href="https://www.facebook.com/1fitapp/" target="_blank" rel="noreferrer">
                  <Facebook />
                </a>
              </li>
            </ul>

            <a href="tel:++77057772776" className="footer__tel">
              +7 (705) 777-27-76
            </a>
            <p className="footer__address">
              Алматы,​ БЦ LOTOS, Макатаева, 117а, <br />3 этаж, 310 офис
            </p>
            <a href="mailto:support@1fit.app" className="footer__mail">
              support@1fit.app
            </a>
          </div>
        </div>

        <p className="footer__copyright">
          Copyright ©1Fit, {new Date().getFullYear()} Все права защищены.
        </p>
      </footer>
    </>
  );
};
