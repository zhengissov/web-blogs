import { useTranslation } from 'next-i18next';
import { DownloadAppLinks } from './links';

export function DownloadApp() {
  const { t } = useTranslation('download-app');

  return (
    <div className="download-app">
      <p className="download-app__title">{t('download-app')}</p>
      <p className="download-app__description">{t('download-app-description')}</p>
      <DownloadAppLinks />
    </div>
  );
}
