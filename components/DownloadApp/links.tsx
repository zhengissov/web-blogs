import AppStore from 'assets/AppStore';
import GooglePlay from 'assets/GooglePlay';

const appStoreURL =
  '//itunes.apple.com/us/app/1fit-%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D1%84%D0%B8%D1%82%D0%BD%D0%B5%D1%81-%D0%B0%D0%B1%D0%BE%D0%BD%D0%B5%D0%BC%D0%B5%D0%BD%D1%82/id1375903148?mt=8&utm_source=Landing&utm_medium=Banner&utm_campaign=ios_desktop';
const googlePlayURL = '//play.google.com/store/apps/details?id=com.codebusters.onefit&hl=ru';

export function DownloadAppLinks() {
  return (
    <div className="download-app__links">
      <a className="download-app__link" href={appStoreURL} target="_blank" rel="noreferrer">
        <AppStore />
      </a>
      <a className="download-app__link" href={googlePlayURL} target="_blank" rel="noreferrer">
        <GooglePlay />
      </a>
    </div>
  );
}
