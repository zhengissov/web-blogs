export const parseLevel = (level: string) => {
  switch (level) {
    case 'easy':
      return 'NEW';
    case 'normal':
      return 'MEDIUM';
    case 'hard':
      return 'PRO';
    case 'NEW':
      return 'easy';
    case 'MEDIUM':
      return 'normal';
    case 'PRO':
      return 'hard';
    default:
      return level;
  }
};
