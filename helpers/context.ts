import { createContext } from 'react';

export const SportLevelContext = createContext({
  level: 'none',
  setLevel: (l: string) => {
    console.log(l);
  },
});
